# WindhawkMods
Mods for Windhawk

## Classic Theme Explorer
Classic Theme mitigations for Explorer as a Windhawk mod.

Based on [ExplorerPatcher](https://github.com/valinet/ExplorerPatcher), [SimpleClassicTheme.FileExplorerHook](https://github.com/AEAEAEAE4343/SimpleClassicTheme.FileExplorerHook) and the AutoHotkey scripts that came before it.

## Classic Theme Windows
Forces Classic Theme on all Windows

Originally written by WinClassic user Travis. Added DWM disabler ala BasicThemer2 for classic titlebars and Classic File Picker.

## ~~Classic File Picker~~
~~Restores pre-Vista file picker. Requires Classic Theme Windows mod or else Windows just upgrades the dialog.~~

Merged into Classic Theme Windows since its required to work anyways.