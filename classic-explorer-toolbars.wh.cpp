// ==WindhawkMod==
// @id              classic-explorer-toolbars
// @name            Classic Explorer Toolbars
// @description     Restyles Explorer toolbars to fit in with classic theme
// @version         1.0.1
// @author          Cynosphere
// @github          https://github.com/Cynosphere
// @homepage        https://c7.pm
// @include         explorer.exe
// @compilerOptions -lcomdlg32
// ==/WindhawkMod==

// ==WindhawkModReadme==
/*
# Classic Explorer Toolbars
Restyles Explorer toolbars to fit in with classic theme

## Changelog
**1.0.1** (2023/12/10)
- Fix applying to taskbar

**1.0.0** (2023/11/27)
- Initial release
*/
// ==/WindhawkModReadme==

#include <windhawk_utils.h>

#ifdef _WIN64
#   define THISCALL  __cdecl
#   define STHISCALL L"__cdecl"
#else
#   define THISCALL  __thiscall
#   define STHISCALL L"__thiscall"
#endif

typedef long (THISCALL *BandSite__Initialize_t)(int* pThis, HWND hWnd);
BandSite__Initialize_t CBandSite__Initialize_orig;
long THISCALL CBandSite__Initialize_hook(int* pThis, HWND hWnd) {
    long ret = CBandSite__Initialize_orig(pThis, hWnd);

    // FIXME: pointer math bad!
    HWND barHwnd = *((HWND *)pThis + 17);

    if (barHwnd) {
        HWND parentHwnd = GetParent(barHwnd);
        WCHAR lpParCls[256];
        GetClassNameW(parentHwnd, lpParCls, 256);

        if (!wcscmp(lpParCls, L"WorkerW")) {
            LONG style = GetWindowLongPtrW(barHwnd, GWL_STYLE);
            style |= RBS_BANDBORDERS;
            style |= WS_BORDER;
            SetWindowLongPtrW(barHwnd, GWL_STYLE, style);
        }
    }

    return ret;
}

BOOL Wh_ModInit() {
    HMODULE hExplorerFrame = LoadLibraryW(L"ExplorerFrame.dll");
    if (!hExplorerFrame)
    {
        Wh_Log(L"Failed to load ExplorerFrame.dll");
        return FALSE;
    }

    const WindhawkUtils::SYMBOL_HOOK hooks[] = {
        {
            {
                L"protected: virtual long "
                STHISCALL
                L" CBandSite::_Initialize(struct HWND__ *)"
            },
            &CBandSite__Initialize_orig,
            CBandSite__Initialize_hook,
            false
        }
    };

    if (!WindhawkUtils::HookSymbols(
        hExplorerFrame,
        hooks,
        ARRAYSIZE(hooks)
    ))
    {
        Wh_Log(L"Failed to hook CBandSite::_Initialize");
        return FALSE;
    }

    return TRUE;
}
