// ==WindhawkMod==
// @id              hide-navigation-bar
// @name            Hide Navigation Bar
// @description     Hide navigation bar by removing it entirely
// @version         0.1
// @author          Cynosphere
// @github          https://github.com/Cynosphere
// @homepage        https://c7.pm
// @include         explorer.exe
// @compilerOptions -lcomdlg32
// ==/WindhawkMod==

// ==WindhawkModReadme==
/*
# Hide Navigation Bar
*/
// ==/WindhawkModReadme==

#include <windhawk_utils.h>

#ifdef _WIN64
#   define THISCALL  __cdecl
#   define STHISCALL L"__cdecl"
#else
#   define THISCALL  __thiscall
#   define STHISCALL L"__thiscall"
#endif

typedef long (THISCALL *CNavBar__CreateBar_t)(void *);
CNavBar__CreateBar_t CNavBar__CreateBar_orig;
long THISCALL CNavBar__CreateBar_hook(void *pThis) {
    return 0;
}

BOOL Wh_ModInit() {
    HMODULE hExplorerFrame = LoadLibraryW(L"ExplorerFrame.dll");
    if (!hExplorerFrame)
    {
        Wh_Log(L"Failed to load ExplorerFrame.dll");
        return FALSE;
    }

    const WindhawkUtils::SYMBOL_HOOK hooks[] = {
        {
            {
                L"protected: long "
                STHISCALL
                L" CNavBar::_CreateBar(void)"
            },
            &CNavBar__CreateBar_orig,
            CNavBar__CreateBar_hook,
            false
        }
    };

    if (!WindhawkUtils::HookSymbols(
        hExplorerFrame,
        hooks,
        ARRAYSIZE(hooks)
    ))
    {
        Wh_Log(L"Failed to hook CAddressBand::_CreateAddressBand");
        return FALSE;
    }

    return TRUE;
}
