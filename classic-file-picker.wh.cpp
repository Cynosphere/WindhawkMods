// ==WindhawkMod==
// @id              classic-file-picker
// @name            Classic File Picker
// @description     Redirect the Windows Vista+ file picker to the Windows XP one
// @version         0.1
// @author          Cynosphere
// @github          https://github.com/cynosphere
// @homepage        https://c7.pm
// @include         *
// @compilerOptions -lole32 -lcomdlg32
// ==/WindhawkMod==

// ==WindhawkModReadme==
/*
# Classic File Picker
*/
// ==/WindhawkModReadme==

template<class S>
CLSID CreateGUID(const S& hexString)
{
    CLSID clsid;
    CLSIDFromString(hexString, &clsid);

    return clsid;
}

const CLSID CLSID_FileOpenDialog = CreateGUID(L"{DC1C5A9C-E88A-4dde-A5A1-60F82A20AEF7}");
const CLSID CLSID_FileOpenDialogLegacy = CreateGUID(L"{725F645B-EAED-4fc5-B1C5-D9AD0ACCBA5E}");
const CLSID CLSID_FileSaveDialog = CreateGUID(L"{c0b4e2f3-ba21-4773-8dba-335ec946eb8b}");
const CLSID CLSID_FileSaveDialogLegacy = CreateGUID(L"{AF02484C-A0A9-4669-9051-058AB12B9195}");

using CoCreateInstance_t = decltype(&CoCreateInstance);
CoCreateInstance_t CoCreateInstance_Original;
HRESULT WINAPI CoCreateInstance_Hook(REFCLSID rclsid, LPUNKNOWN pUnkOuter, DWORD dwClsContext, REFIID riid, LPVOID *ppv) {
    if (rclsid == CLSID_FileOpenDialog) {
        return CoCreateInstance_Original(CLSID_FileOpenDialogLegacy, pUnkOuter, dwClsContext, riid, ppv);
    } else if (rclsid == CLSID_FileSaveDialog) {
        return CoCreateInstance_Original(CLSID_FileSaveDialogLegacy, pUnkOuter, dwClsContext, riid, ppv);
    }

    return CoCreateInstance_Original(rclsid, pUnkOuter, dwClsContext, riid, ppv);
}

BOOL Wh_ModInit() {
    Wh_SetFunctionHook((void*)CoCreateInstance, (void*)CoCreateInstance_Hook,
                       (void**)&CoCreateInstance_Original);

    return TRUE;
}

void Wh_ModUninit() {}